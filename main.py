import sys
import eel
import os

timeout_argument = 1

eel.init('web', allowed_extensions=['.js', '.html'])


@eel.expose
def get_item(p):
    print(type(bytes(p, 'UTF-8')))
    print(p)


@eel.expose
def get_choice_category(c):
    eel.get_item_from_folder(sorted([f for f in os.listdir('web/media/' + c)]), c)


eel.get_choice(sorted([f for f in os.listdir('web/media/choice')]))

#timeout_amount = sys.argv[timeout_argument]

#eel.get_time_out(timeout_amount)
eel.start('index.html')




