choiceList = []
juiceList = []
iceList = []

timeoutAmount = 5000

//send file to the back-end
function send_to_back_end(item){
    loadVideo();
    setTimeout(() => {reset()}, timeoutAmount);

    eel.get_item(item);
}

function enableBackBtn(){
   document.getElementById("backBtn").style.visibility = "visible";
}

function disableBackBtn(){
   document.getElementById("backBtn").style.visibility = "hidden";
}

function get_choice_category(choice){
    eel.get_choice_category(choice);
}

function reset(){
    disableBackBtn();
    document.body.style.filter = "blur(0px)";
    document.getElementById("mc").innerHTML = choiceList.join('');
}

function loadVideo(){
    disableBackBtn();

    /// doesn work yet..
    document.body.style.filter = "blur(50px)";
    spinner = `
        <video style="width: 75%; height: 75% ;position: center;" autoplay="autoplay" loop><source src="media/video/video.mp4" type="video/mp4"></video>
        `;

    document.getElementById("mc").innerHTML = spinner;
}

eel.expose(get_time_out);
function get_time_out(timeout_amount){
   timeoutAmount = timeout_amount;
}


//get data from the back-end
// dynamically constructing html code on basis of the available files in media folder
eel.expose(get_item_from_folder);
function get_item_from_folder(files, category) {
   enableBackBtn();
   if(category === "juice"){
        createJuice(files)
   }

   if(category === "ice"){
        createIce(files)
   }
}

function showBeginPage(){
     disableBackBtn();
     document.getElementById("mc").innerHTML = choiceList.join('');
}

eel.expose(get_choice);
function get_choice(choices){
    disableBackBtn();
    if(choiceList.length){
        document.getElementById("mc").innerHTML = choiceList.join('');
    }else{
        choiceList.push('<ul id="ulId">');

        for(f in choices){
            confirm_text = "Choose";
            file_name = choices[f];

            item = file_name.split('.').slice(0, -1);
            console.log(item);

            liString =
            `<li><div class="onClickTextOverImage" style="background-image:url(media/choice/${file_name})"><a href="#${item}"></a><div id="${item}">${confirm_text}<a onclick="get_choice_category('${item}')"></a></div></div></li>`;

            choiceList.push(liString);
           }

        choiceList.push('</ul>');

        document.getElementById("mc").innerHTML = choiceList.join('');
    }
}

function createIce(files){
     if (iceList.length){
             document.getElementById("mc").innerHTML = iceList.join('');
        }else{
            iceList.push('<ul id="ulId">');

                for(f in files){
                    file_name = files[f];

                    item = file_name.split('.').slice(0, -1);

                    confirm_text = "Order";

                    liString =
                    `<li><div class="onClickTextOverImage" style="background-image:url(media/ice/${file_name})"><a href="#${item}"></a><div id="${item}">${confirm_text}<a onclick="send_to_back_end('${item}')"></a></div></div></li>
                    `;

                    iceList.push(liString);
                   }

                iceList.push('</ul>');

                document.getElementById("mc").innerHTML = iceList.join('');
        }
}

function createJuice(files){

    if (juiceList.length){
         document.getElementById("mc").innerHTML = juiceList.join('');
    }else{
        juiceList.push('<ul id="ulId">');

            for(f in files){
                file_name = files[f];

                item = file_name.split('.').slice(0, -1);

                confirm_text = "Order";

                liString =
                `<li><div class="onClickTextOverImage" style="background-image:url(media/juice/${file_name})">
                <a href="#${item}"></a><div id="${item}">${confirm_text}<a onclick="send_to_back_end('${item}')"></a></div></div></li>
                `;

                juiceList.push(liString);
               }

            juiceList.push('</ul>');

            document.getElementById("mc").innerHTML = juiceList.join('');
    }
}

